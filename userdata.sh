#!/bin/bash -xe

exec > >(tee /var/log/cloud-init-output.log) 2> >(logger -t user-data -s)

SSM_DB_PASSWORD="/ghost/dbpassw"
TOKEN=$(curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600")
REGION=$(/usr/bin/curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/placement/availability-zone | sed 's/[a-z]$//')
LB_DNS_NAME=$(aws elbv2 describe-load-balancers --region $REGION --query "LoadBalancers[*].DNSName" --output text)
RDS_HOST=$(aws rds describe-db-instances --region $REGION --query "DBInstances[*].Endpoint.Address" --output text)
DB_PASSWORD=$(aws ssm get-parameter --name $SSM_DB_PASSWORD --query Parameter.Value --with-decryption --region $REGION --output text)
EFS_ID=$(aws efs describe-file-systems --region $REGION --output text | grep 'ghost_content' | grep -v 'ghost_content_2' | awk '/^FILESYSTEMS/ {print $6}')

echo $EFS_ID > 12345.txt

### Install pre-reqs
curl -sL https://rpm.nodesource.com/setup_14.x | sudo bash -
yum install -y nodejs amazon-efs-utils
npm install ghost-cli@latest -g

#kill -9 $(fuser 2368/tcp 2>/dev/null)
adduser ghost_user
usermod -aG wheel ghost_user
cd /home/ghost_user/

sudo -u ghost_user ghost install [4.12.1] local

### EFS mount
echo $EFS_ID
mkdir -p /home/ghost_user/ghost/content/data
mount -t efs -o tls $EFS_ID:/ /home/ghost_user/ghost/content

cp -R content/* ghost/content/

cat << EOF > config.development.json

{
  "url": "http://${LB_DNS_NAME}",
  "server": {
    "port": 2368,
    "host": "0.0.0.0"
  },
  "database": {
    "client": "mysql",
    "connection": {
        "host": "$RDS_HOST",
        "port": 3306,
        "user": "admin",
        "password": "$DB_PASSWORD",
        "database": "ghost"
    }
  },
  "mail": {
    "transport": "Direct"
  },
  "logging": {
    "transports": [
      "file",
      "stdout"
    ]
  },
  "process": "local",
  "paths": {
    "contentPath": "/home/ghost_user/ghost/content"
  }
}
EOF

sudo -u ghost_user ghost stop
sudo -u ghost_user ghost start