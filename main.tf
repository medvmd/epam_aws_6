provider "aws" {
  region = "us-east-1"
}

#VPC
resource "aws_vpc" "cloudx" {
  cidr_block           = "10.10.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "cloudx"
  }
}

#Public Subnets
resource "aws_subnet" "public_a" {
  vpc_id            = aws_vpc.cloudx.id
  cidr_block        = "10.10.1.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "public_a"
  }
}

resource "aws_subnet" "public_b" {
  vpc_id            = aws_vpc.cloudx.id
  cidr_block        = "10.10.2.0/24"
  availability_zone = "us-east-1b"
  tags = {
    Name = "public_b"
  }
}

resource "aws_subnet" "public_c" {
  vpc_id            = aws_vpc.cloudx.id
  cidr_block        = "10.10.3.0/24"
  availability_zone = "us-east-1c"
  tags = {
    Name = "public_c"
  }
}

#Internet Gateway
resource "aws_internet_gateway" "cloudx_igw" {
  vpc_id = aws_vpc.cloudx.id
  tags = {
    Name = "cloudx-igw"
  }
}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.cloudx.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cloudx_igw.id
  }
  tags = {
    Name = "public_rt"
  }
}

resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "public_c" {
  subnet_id      = aws_subnet.public_c.id
  route_table_id = aws_route_table.public_rt.id
}

#Security Groups
resource "aws_security_group" "bastion" {
  vpc_id = aws_vpc.cloudx.id
  description = "allows access to bastion"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["185.115.5.89/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "bastion"
  }
}

resource "aws_security_group" "alb" {
  vpc_id = aws_vpc.cloudx.id
  description = "allows access to alb"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["185.115.5.89/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb"
  }
}

resource "aws_security_group" "ec2_pool" {
  vpc_id = aws_vpc.cloudx.id
  description = "allows access to ec2 instances"

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion.id]
  }

  ingress {
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.cloudx.cidr_block]
  }

  ingress {
    from_port   = 2368
    to_port     = 2368
    protocol    = "tcp"
    security_groups = [aws_security_group.alb.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ec2_pool"
  }
}

resource "aws_security_group" "efs" {
  vpc_id = aws_vpc.cloudx.id
  description = "defines access to efs mount points"

  ingress {
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = [aws_security_group.ec2_pool.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.cloudx.cidr_block]
  }

  tags = {
    Name = "efs"
  }
}

#IAM
resource "aws_iam_role" "ghost_app" {
  name = "ghost_app"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "ghost_app_policy" {
  name   = "ghost_app_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Describe*",
        "ec2:DescribeInstances",
        "elasticfilesystem:DescribeFileSystems",
        "elasticfilesystem:ClientMount",
        "ssm:GetParameter*",
        "secretsmanager:GetSecretValue",
        "kms:Decrypt",
        "elasticfilesystem:ClientWrite",
        "elasticloadbalancing:DescribeLoadBalancers",
        "rds:DescribeDBInstances"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ghost_app_attach" {
  role       = aws_iam_role.ghost_app.name
  policy_arn = aws_iam_policy.ghost_app_policy.arn
}

resource "aws_iam_instance_profile" "ghost_app" {
  name = "ghost_app"
  role = aws_iam_role.ghost_app.name
}

# FS
resource "aws_efs_file_system" "ghost_content" {
  creation_token = "ghost_content"
  tags = {
    Name = "ghost_content"
  }
}

resource "aws_efs_mount_target" "ghost_content_a" {
  file_system_id  = aws_efs_file_system.ghost_content.id
  subnet_id       = aws_subnet.public_a.id
  security_groups = [aws_security_group.efs.id]
}

resource "aws_efs_mount_target" "ghost_content_b" {
  file_system_id  = aws_efs_file_system.ghost_content.id
  subnet_id       = aws_subnet.public_b.id
  security_groups = [aws_security_group.efs.id]
}

resource "aws_efs_mount_target" "ghost_content_c" {
  file_system_id  = aws_efs_file_system.ghost_content.id
  subnet_id       = aws_subnet.public_c.id
  security_groups = [aws_security_group.efs.id]
}

#Load Balancer
resource "aws_lb" "ghost_alb" {
  name               = "ghost-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb.id]
  subnets            = [aws_subnet.public_a.id, aws_subnet.public_b.id, aws_subnet.public_c.id]

  tags = {
    Name = "ghost-alb"
  }
}

resource "aws_lb_target_group" "ghost_ec2" {
  name     = "ghost-ec2"
  port     = 2368
  protocol = "HTTP"
  vpc_id   = aws_vpc.cloudx.id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200-299"
    interval            = 30
    timeout             = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  tags = {
    Name = "ghost-ec2"
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.ghost_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ghost_ec2.arn
  }
}


variable "LB_DNS_NAME" {
  description = "ALB DNS name"
  type        = string
}

#Launch Template
data "template_file" "user_data" {
  template = file("${path.module}/userdata.sh")
  
  vars = {
    LB_DNS_NAME = var.LB_DNS_NAME 
  } 
}

resource "aws_launch_template" "ghost" {
  name_prefix   = "ghost"
  image_id      = "ami-0d94353f7bad10668"  # Amazon Linux 2 AMI
  instance_type = "t2.micro"
  key_name      = "ghost-ec2-pool"

  iam_instance_profile {
    name = aws_iam_instance_profile.ghost_app.name
  }

  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [aws_security_group.ec2_pool.id]
  }

  user_data = base64encode(data.template_file.user_data.rendered)
}

#Auto Scaling Group
resource "aws_autoscaling_group" "ghost_ec2_pool" {
  desired_capacity    = 1
  max_size            = 1
  min_size            = 1
  vpc_zone_identifier = [aws_subnet.public_a.id, aws_subnet.public_b.id, aws_subnet.public_c.id]
  launch_template {
    id      = aws_launch_template.ghost.id
    version = "${aws_launch_template.ghost.latest_version}"  # Fix: Use latest version dynamically
  }
  target_group_arns = [aws_lb_target_group.ghost_ec2.arn]

  tag {
    key                 = "Name"
    value               = "ghost-ec2-pool"
    propagate_at_launch = true
  }
}

#Bastion
resource "aws_instance" "bastion" {
  ami                    = "ami-0d94353f7bad10668"  # Amazon Linux 2 AMI
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.public_a.id
  associate_public_ip_address = true
  security_groups        = [aws_security_group.bastion.id]
  key_name               = "ghost-ec2-pool"

  tags = {
    Name = "bastion"
  }
}

